/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author KHANOMMECOM
 */
public class Deletedatabase {
    public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:Dcoffee.db";
        //connect database 
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connect to SQLite has been establish");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }
        
        //select database
        String sql = "DELETE FROM category WHERE category_id=?"; 
         try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1,7);
            
            
            int status = stmt.executeUpdate();
            
           
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        
        
        //close database
        if(conn!=null){
                try {
                    conn.close();
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
    }
}
